package com.vlist.model;

public class Admin_Login
{
	private String uid;
	private String password;
	
	public Admin_Login() {
		
	}
	public Admin_Login(String uid, String password) {
		super();
		this.uid = uid;
		this.password = password;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	

}
