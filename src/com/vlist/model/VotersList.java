package com.vlist.model;

public class VotersList {
private Integer vid;
private String vname;
private String vgender;
private String vadd;

public VotersList() {
}

public VotersList(Integer vid, String vname, String vgender, String vadd) {
	super();
	this.vid = vid;
	this.vname = vname;
	this.vgender = vgender;
	this.vadd = vadd;
}

public Integer getVid() {
	return vid;
}

public void setVid(Integer vid) {
	this.vid = vid;
}

public String getVname() {
	return vname;
}

public void setVname(String vname) {
	this.vname = vname;
}

public String getVgender() {
	return vgender;
}

public void setVgender(String vgender) {
	this.vgender = vgender;
}

public String getVadd() {
	return vadd;
}

public void setVadd(String vadd) {
	this.vadd = vadd;
}






}
