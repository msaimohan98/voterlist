package com.vlist.view;

import java.util.List;
import java.util.Scanner;

import com.vlist.controller.AdminController;
import com.vlist.model.VotersList;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter user id");
		String uid = sc.nextLine();
		System.out.println("enter password");
		String password = sc.nextLine();
		AdminController controller = new AdminController();
		controller.admin1Auth(uid, password);
		int result = 0;

		result = controller.admin1Auth(uid, password);
		if (result > 0) {
			System.out.println("welcome"  + uid + "to admin page");
			int con;
			do {
			System.out.println("1.view all list details" + " 2.Add list details " + " 3.Edit list details "
					+ " 4.Remove list Details");

			int choice = sc.nextInt();
			switch (choice) {
			case 1:
				List<VotersList> list = controller.viewvoterslist();
				if (list.size() > 0) {
					System.out.println("printing all the view details");

					for (VotersList s : list) {
						System.out
								.println(s.getVid() + ", " + s.getVname() + ", " + s.getVgender() + ", " + s.getVadd());
					}
				} else {
					System.out.println("No records!");
				}
				break;
			case 2:

				System.out.println("Adding the List details");
				System.out.println("Enter the member Id");
				int vid = sc.nextInt();
				sc.nextLine();
				System.out.println("Enter the member name");
				String vname = sc.nextLine();
				System.out.println("enter the  gender");
				String vgender = sc.nextLine();
				System.out.println("enter the member address");
				String vadd = sc.nextLine();

				result = controller.addList(vid, vname, vgender, vadd);
				if (result > 0) {
					System.out.println("Successfully added the list details");
				} else {
					System.out.println("Unsuccessfull to add the list details");
				}

				break;
			case 3:

				System.out.println("Editing the details");
				System.out.println("Enter the Id");
				vid = sc.nextInt();
				sc.nextLine();
				System.out.println("Enter the member name");
				vname = sc.nextLine();
				System.out.println("enter your gender");
				vgender = sc.nextLine();
				result = controller.editList( vname, vgender,vid);
				if (result == 0) {
					System.out.println("Successfully edited the list details");
				} else {
					System.out.println("Unsuccessfull to edited the list details");
				}

				break;

			case 4:

				System.out.println("Removing the details");
				System.out.println("Enter the Id");
				vid = sc.nextInt();

				result = controller.removeList(vid);

				if (result>0) {
					System.out.println("Successfully removed the list details");
				} else {
					System.out.println("Unsuccessfull to remove the list details");
				}

				break;
			default:
				System.out.println("Invalid section!");
				break;
			
			
			}System.out.println("Do you want to continue press 1");
             con=sc.nextInt();
			}while(con==1);
			
		} else {
			System.out.println("Please! check your userId or Password");
		}

		sc.close();

	}

}
