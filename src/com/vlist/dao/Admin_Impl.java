package com.vlist.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.print.CancelablePrintJob;

import com.vlist.model.Admin_Login;
import com.vlist.model.VotersList;
import com.vlist.util.Db;
import com.vlist.util.Query;

public class Admin_Impl implements Admin_Dao {
	PreparedStatement pst;
	ResultSet rs;
	int result;

	@Override
	public int admin1Auth(Admin_Login admin) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.admin1Auth);
			pst.setString(1, admin.getUid());
			pst.setString(2, admin.getPassword());
			rs = pst.executeQuery();
			while (rs.next()) {
				result++;
			}
		} catch (ClassNotFoundException | SQLException e) {
			// e.printStackTrace();
			System.out.println("Exception occurs in admin1 Authentication");
		} finally {
			try {
				pst.close();
				rs.close();
				Db.getConnection().close();
			} catch (SQLException | ClassNotFoundException e) {

			}

		}

		return result;

	}

	@Override
	public List<VotersList> viewvoterslist() {
		List<VotersList> list = new ArrayList<VotersList>();
		try {
			pst = Db.getConnection().prepareStatement(Query.viewList);

			rs = pst.executeQuery();
			while (rs.next()) {
				VotersList vlist = new VotersList(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
				list.add(vlist);
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in view all list");
		} finally {

			try {
				rs.close();
				pst.close();
				Db.getConnection().close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}
		return list;

	}

	@Override
	public int  addList(VotersList vlist) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.addList);
			pst.setInt(1, vlist.getVid());
			pst.setString(2, vlist.getVname());
			pst.setString(3, vlist.getVgender());
			pst.setString(4, vlist.getVadd());
			result = pst.executeUpdate();

		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in add_Details");

		} finally {
			try {
				pst.close();
				Db.getConnection().close();
			} catch (SQLException | ClassNotFoundException e) {

			}

		}
		return result;
	}

	@Override
	public int  editList(VotersList vlist) {

		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.editList);
			pst.setString(1, vlist.getVname());
			pst.setString(2, vlist.getVgender());
			pst.setInt(3, vlist.getVid());

			result = pst.executeUpdate();

		} catch (ClassNotFoundException | SQLException e) {
		
			 System.out.println("Exception occurs in edit list");

		} finally {
			try {
				pst.close();
				Db.getConnection().close();
			} catch (SQLException | ClassNotFoundException e) {

			}
		}
		return result;
	}

	@Override
	public int removeList(VotersList vlist) {
		result = 0;
		try {
			pst=Db.getConnection().prepareStatement(Query.removeList);
			pst.setInt(1,vlist.getVid());
			result= pst.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
			
//			 System.out.println("Exception occurs in remove list");
		}finally {
			try {
				pst.close();
				Db.getConnection().close();
			} catch (SQLException | ClassNotFoundException e) {

			}
		}
			
		return result;
	}

	

}
