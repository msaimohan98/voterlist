package com.vlist.dao;

import java.util.List;

import com.vlist.model.Admin_Login;
import com.vlist.model.VotersList;

public interface Admin_Dao {
	public int admin1Auth(Admin_Login admin);

	public List<VotersList> viewvoterslist();

	public int addList(VotersList vlist);
	public int editList(VotersList vlist);
	public int removeList(VotersList vlist);
}

