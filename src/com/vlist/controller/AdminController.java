package com.vlist.controller;

import java.util.List;

import com.vlist.dao.Admin_Dao;
import com.vlist.dao.Admin_Impl;
import com.vlist.model.Admin_Login;
import com.vlist.model.VotersList;

public class AdminController {
	int result;
	Admin_Dao dao = new Admin_Impl();

	public int admin1Auth(String uid, String password) {
		result = 0;
		Admin_Login admin = new Admin_Login(uid, password);
		return dao.admin1Auth(admin);

	}

	public List<VotersList> viewvoterslist() {
		return dao.viewvoterslist();

	}

	public int addList(int vid, String vname, String vgender, String vadd) {

		VotersList list1 = new VotersList(vid, vname, vgender, vadd);

		return dao.addList(list1);

	}

	public int editList( String vname, String vgender,int vid) {

		VotersList list2 = new VotersList();
	
		list2.setVname(vname);
		list2.setVgender(vgender);
		list2.setVid(vid);
		

		return dao.editList(list2);

	}

	public int removeList(int vid) {

		VotersList list3 = new VotersList();
		list3.setVid(vid);

		return dao.removeList(list3);

	}

}
